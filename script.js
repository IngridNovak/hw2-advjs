//1. try...catch використовують для того, щоб замість падіння скрипта з виведенням помилки в консоль, виводилось щось більш логічне. Наприклад,
//його часто використовують при роботі з JSON та JSON parse, щоб при наявності помилки в даних, скрипт не просто падав, а виводив логічне повідомлення
//про помилку та вказував де конкретно виникла помилка.
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

let mustHave = ['author', 'name', 'price'];


let root = createDOMElement("div", {'id':"root"});
let ul = createDOMElement("UL",{'id':'ul'}, root);
document.body.prepend(root);


books.forEach((item) => {

    try {
        checkProperties(item);

        let li = createDOMElement("LI", {'body': JSON.stringify(item)}, ul);
    } catch (err){
        console.log('Error:'+ 'in ' + JSON.stringify(item) + ' ' + err.message);

    }
})
function checkProperties(object) {
    mustHave.forEach(elem => {
        if (!object.hasOwnProperty(elem)) {
            throw new SyntaxError(`Немає ${elem}`);
        }
    })

}

function  createDOMElement(tag, params, parentElement) {
    let elem = document.createElement(tag);
    for (const key in params) {
        elem.setAttribute(key, params[key]);
        if (key === 'body'){
            elem.innerHTML = params[key];
        }
    }
    if (parentElement !== undefined) {
        parentElement.append(elem);
    }
        return elem;
    }

